package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
    "io/ioutil"
	"net/http"
	"time"
)

const token string = "NzA5MTA2NjQ2NzE0MTU1MDA4.XrhLUw.goIufYht8BSX8BjHZ1h3FfkYrFk"
var BotID string
var ContinuePulling bool = true

func main(){
	// TODO - Create an secret for this token
	discordClient, err := discordgo.New("Bot " + "NzA5MTA2NjQ2NzE0MTU1MDA4.XrhLUw.goIufYht8BSX8BjHZ1h3FfkYrFk")
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}
	user, err := discordClient.User("@me")
	if err != nil {
		fmt.Println("error userCreation session,", err)
		return
	}
	
	BotID = user.ID

	discordClient.AddHandler(messageHandler)

	err = discordClient.Open()
	if err != nil {
		fmt.Println("error open discordClient session,", err)
		return
	}
	fmt.Println("Bot is running!")

	<-make(chan struct{})
	return
}

func messageHandler(discordSession *discordgo.Session, messageReceived *discordgo.MessageCreate) {
	if messageReceived.Author.ID == BotID {
		return
	}

	if messageReceived.Content == "#RegisterChannel"{
		_, _ = discordSession.ChannelMessageSend(messageReceived.ChannelID, "Os Valores do Albion Gold Serão postados neste canal")
		albionGoldPulling(discordSession, messageReceived.ChannelID)
	}

	if messageReceived.Content == "#StopPulling"{
		ContinuePulling = false
	}

	fmt.Println(messageReceived.Content)	
}

func albionGoldPulling(discordSession *discordgo.Session, channelID string){
	fmt.Println("Starting the Pulling...")
	for ContinuePulling{
		//Testando pegar os dados da api 
    	response, err := http.Get("https://www.albion-online-data.com/api/v2/stats/gold?count=1")
    	if err != nil {
        	fmt.Printf("The HTTP request failed with error %s\n", err)
    	} else {
        	data, _ := ioutil.ReadAll(response.Body)
			_, _ = discordSession.ChannelMessageSend(channelID, string(data))
    	}
		time.Sleep(60 * time.Minute)
	}
	_, _ = discordSession.ChannelMessageSend(channelID, "Parei!")
}